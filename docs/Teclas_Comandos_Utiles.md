#Combinaciones de teclas y comandos útiles

Opción | Descripción
-- | --
**?** | Muestra una descripción de comandos
**exit** | Vuelve a un nivel anterior
**No** | La sentencia “no” anula el comando introducido.
**enable** | Ingresa en modo privilegiado
**Control-Z** | Vuelve a la raíz
**disable** | Sale del modo privilegiado
**reload** | Reinicia el router
**setup** | Ingresa al diálogo de configuración del sistema. Sale con CTRL+C
**Clock set** | modifica fecha y hora del equipo
**CTRL+Shift+6** | Corta traceroute

##Escritura rápida

Dos opciones de la consola que ayudan a entrar comandos mucho más rápido y más fácil:

La tecla “Tab”, al escribir 2 o 3 letras del comando con Tab se completa el nombre del comando a utilizar.

Las abreviaturas de los nombres de los comandos también son válidas de utilizar, no es necesario escribir todo el nombre y el comando tendrá efecto de igual manera.
