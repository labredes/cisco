# Estructura de comandos

Puede usar el modo de configuración (diálogo setup) pero esto es sólo para ayudarlo a configurar el dispositivo con los comandos Cisco IOS. El modo setup sólo le permitirá configurar el router con las funciones básicas y sin ninguna función avanzada.
El sistema operativo de cisco se denomina IOS, cuya arquitectura define distintos modos de configuración. Cada modo de configuración permite realizar diferentes acciones y/o configuraciones. Los modos más utilizados son: 


![](imagenes/modos.jpg)

## Modos de un Router

Modo de configuración inicial (SETUP) | Modo RXBOOT
-- | --
Diálogo con indicadores utilizado para establecer una configuración inicial. | Recuperación de desastres en caso de pérdida de la contraseña o si el sistema operativo se borra accidentalmente de la Flash.


Modo EXEC del usuario | Modo EXEC privilegiado |
-- | --
Análisis limitado del dispositivo. Acceso remoto. | Análisis detallado del dispositivo. Depuración y prueba. Manipulación de archivos. Acceso remoto.
equipo> | equipo# |

Modo de configuración global | Otros modos de configuración
-- | --
Comandos de configuración simple. | Configuraciones complejas y de múltiples líneas. 
equipo(config)# | equipo(config - mode)#

modo de configuración de las interfaces | modo de configuración de la líneas de ingreso
-- | --
equipo(config-if)# | equipo(config-line)# | 
