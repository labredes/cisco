# Estructura de archivos en un equipo Cisco

## Tipos de memorias

**NVRAM**: Memoria RAM no volátil, almacena la copia de respaldo de:

* archivo de configuración
* archivo de configuración de inicio del router

**ROM**: Contiene diagnósticos de encendido, un programa bootstrap y software del sistema operativo. Las actualizaciones del software en la ROM requieren la eliminación y el reemplazo de chips enchufables en la CPU

**RAM/DRAM**: Almacena los siguientes datos:

* Tablas de enrutamiento
* Caché ARP
* Caché de conmutación rápida
* Búfering de paquetes (RAM compartida)
* Colas de espera de paquetes.

**Flash**: ROM borrable y reprogramable

* Retiene la imagen y el microcódigo del sistema operativo.
* Activa las actualizaciones del software sin eliminar o reemplazar los chips del procesador.
* Puede almacenar múltiples versiones del software IOS

**Interfaces**: Conexiones de red, en la motherboard o en módulos de interfaz separados, a través de las cuales los paquetes entran y salen de un router

## Comandos de estados de un router

![](imagenes/ComandosEstados.png)
