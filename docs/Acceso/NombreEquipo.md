# Asignación de un nombre un equipo

```
equipo#configure terminal
equipo(config)#hostname nombre
nombre(config)#
```

Donde: | |
-- | --
**nombre** = nombre a configurar en el equipo | |
