# Configuración de protocolo de Trunking de VLAN (VTP)

Uno de los switches debe estar en modo “server” y el otro en modo “cliente”:

Modo Server: envía actualizaciones de vlans a switches configurados como clientes que pertenecen a su mismo dominio. 

Los switches cliente deben tener configurado el mismo dominio que el servidor. Éstos no pueden crear vlans pero si asignar puertos. 

En el modo transparente se pueden crear vlans y reenviar, si es caso necesario, las vlan de un Server. 

La configuración de password es opcional y todos deberán tener el mismo si se utiliza. 

La versión debe ser la misma en todos los switch que compartan un dominio.

```
Switch(config)# vtp mode modo
Switch(config)# vtp domain nombre
Switch(config)# vtp password contraseña
Switch(config)# vtp version 2
```

Donde: | |
-- | --
**modo** = modo de funcionamiento: server/client/transparent | **nombre** = nombre del dominio dentro del servidor
**contraseña** = contraseña asociada al dominio | 
