# Puertos en modo acceso

``` Cisco
Switch> enable
Switch# configure terminal
Switch(config)# interface "interfaz" "#i"
Switch(config-if)# switchport mode access
Switch(config-if)# switchport access vlan "#v"
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **#i** = número de interfaz
**#v** = número de vlan a la que se accede por la interfaz |
