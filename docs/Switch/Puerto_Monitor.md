# Puerto en modo monitor

## Captura sin etiquetado

```
Switch> enable
Switch#
Switch# configure terminal
Switch(config)# monitor session "x" source interface "y"
Switch(config)# monitor session "x" destination interface "z"
```

Donde: | |
-- | --
**x** = número de sesión | **y** = nombre lógico de la interfaz a capturar
**z** = nombre lógico de la interfaz desde donde se visualiza el tráfico | |

## Captura con etiquetado de vlan

```
Switch> enable
Switch#
Switch# configure terminal
Switch(config)# interface "z"
Switch(config-if)# switchport mode trunk
Switch(config-if)# exit
Switch(config)# monitor session "x" source interface "y"
Switch(config)# monitor session "x" destination interface "z" encapsulation dot1q
```

Donde: | |
-- | --
**x** = número de sesión | **y** = nombre lógico de la interfaz a capturar
**z** = nombre lógico de la interfaz desde donde se visualiza el tráfico | |

## Visualizar configuración de puerto monitor

```
Switch> enable
Switch#
Switch#show monitor session all
```
